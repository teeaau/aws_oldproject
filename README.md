# CloudComputing
Thông tin nhóm 03:
    Tên đề tài: Viết ứng dụng kết hợp nhiều docker với nhau
    Thành Viên:

        Bùi Văn Nghĩa    - 18110324
        Nguyễn Khánh Duy - 18110260
        Lê Ngọc Đoan     - 17133014
# Các bước cài đặt
    Bước 1: Tạo ba EC2(hoặc bất kỳ server nào) để chạy 3 container tương ứng với UI, Backend, Database
    
    Bước 2: Ở EC2 chạy UI cài đặt Gitlab Runner.
    Cấu hình token và url theo: https://gitlab.com/nghiabv120100/cloudcomputing/-/settings/ci_cd
            Ở EC2 chạy Backend, cài đặt maven và Gitlab Runner
    Bước 3: Thay đổi địa chỉ ở các file sau
        backend/src/main/resources/application.properties/ 
            -spring.datasource.url = "Địa chỉ mysql"
        backend/src/main/java/com/cloudcomputing/backend/api/StudentAPI.java/
            # Địa chỉ WebUI
            -@CrossOrigin(origins = "http://ec2-54-144-76-28.compute-1.amazonaws.com:5000/")
        StudentManagement/StudentManagement/Controllers/StudentController.cs/
            # Địa chỉ Backend
            - private string url = "http://ec2-54-221-43-177.compute-1.amazonaws.com:8080";
        StudentManagement/StudentManagement/Views/Student/AddStudent.cshtml/
            # Địa chỉ Backend
            url: 'http://ec2-54-234-171-225.compute-1.amazonaws.com:8080/student/add',
    Bước 4: Thực hiện push project lên gitlab, project sẽ tự động deploy lên server



